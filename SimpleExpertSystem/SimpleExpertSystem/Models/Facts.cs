﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleExpertSystem.Models
{
    public class Facts
    {
        public int id { get; set; }
        public string Text { get; set; }
        [NotMapped]
        public bool Value { get; set; }
        public virtual ICollection<Rule> UsedRules { get; set; }
    }
}