﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleExpertSystem.Models
{
    public class Rule
    {
        public int id { get; set; }
        public string Text { get; set; }
        public virtual ICollection<Facts> Facts { get; set; }
    }
}