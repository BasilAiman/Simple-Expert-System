﻿using SimpleExpertSystem.DataStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SimpleExpertSystem
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static Graph Map;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Map = new Graph();
            System.GC.KeepAlive(Map);
        }
    }
}
