﻿using SimpleExpertSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleExpertSystem.DataStructure
{

    public abstract class Node
    {
        protected object _data;
        public string Text { get; set; }
        public int Weight { get; set; }
        public int id { get; set; }
        public int internal_id { get; set; }
        public List<Node> Neighbours { get; set; }
        public Node()
        {
            Neighbours = new List<Node>();
        
        }
        public void AddToNeighbours(Node node)
        {
            Neighbours.Add(node);
        }
        public void AddToNeighbours(List<Node> node_list)
        {
            Neighbours.AddRange(node_list);
        }

        public void RemoveFromNeighbours(Node node)
        {
            Neighbours.Remove(node);
        }

        public T GetData<T>()
        {
            return (T)this._data;
        }


    }
    public class FactNode : Node
    {
        public FactNode() : base()
        {
            Weight = 0;
        }
        public FactNode(Facts Data)
        {
            _data = Data;
            this.id = Data.id;

            Weight = 0;

            Text = Data.Text;
        }
        public Facts GetData()
        {
            return (Facts)_data;
        }
    }
    public class RuleNode : Node
    {
        public RuleNode() : base()
        {
            Weight = 0;

        }
        public RuleNode(Rule Data)
        {
            _data = Data;
            this.id = Data.id;
            Text = Data.Text;
            Weight = 0;

        }
        public Rule GetData()
        {
            return (Rule)_data;
        }
    }

}