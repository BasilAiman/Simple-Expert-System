﻿using SimpleExpertSystem.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleExpertSystem.DataStructure
{
    public class Graph
    {
        private static int id_tracker = 1;
        ApplicationDbContext context;
        private static List<Node> _Nodes;
        private static List<Rule> _rules;
        private static List<Facts> _facts;
        private static List<int> _used_rule_ids;
        private static List<int> _used_fact_ids;

        private static int Attempts_Tracker = 0;


        public Graph()
        {
            context = new ApplicationDbContext();
            _Nodes = new List<Node>();
            _used_rule_ids = new List<int>();
            _used_fact_ids = new List<int>();

            BuildGraph();

        }


        private void LoadLists()
        {

            _rules = context.Rules.ToList();
            _facts = context.Facts.ToList();

        }
        private void BuildGraph()
        {
            LoadLists();
            RuleNode n;
            //Adding All Rule Nodes Nodes
            foreach (var rule in _rules)
            {
                n = new RuleNode(rule);
                n.internal_id = id_tracker++;
                _Nodes.Add(n);

            }

            //Adding Fact Nodes

            var fact_nodes = new List<FactNode>();
            FactNode node_Fact;
            foreach (var fact in _facts)
            {
                var associated_rules = fact.UsedRules;

                var Rules_Node = _Nodes.Where(x => associated_rules.Contains(x.GetData<Rule>())).ToList();

                node_Fact = new FactNode(fact);
                node_Fact.internal_id = id_tracker++;

                node_Fact.AddToNeighbours(Rules_Node);
                fact_nodes.Add(node_Fact);

            }
            _Nodes.AddRange(fact_nodes);
        }

        internal void Reset()
        {
            context = new ApplicationDbContext();
            _Nodes = new List<Node>();
            _used_rule_ids = new List<int>();
            _used_fact_ids = new List<int>();
            id_tracker = 1;
            Attempts_Tracker = 0;
            BuildGraph();
        }

        private int GetRandomRuleId()
        {
            Random r = new Random();
            return r.Next(1, _rules.Count);
        }
        private int GetRandomFactId()
        {
            Random r = new Random();
            return r.Next(_rules.Count, _rules.Count + _facts.Count + 1);

        }
        private Node GetRandomRule()
        {
            int temp_id = GetRandomRuleId();
            while (_used_rule_ids.Contains(temp_id))
            {
                temp_id = GetRandomRuleId();

            }
            _used_rule_ids.Add(temp_id);

            return _Nodes.Find(x => x.internal_id == temp_id);
        }
        private Node GetRandomFact()
        {
            int temp_id = GetRandomFactId();
            while (_used_fact_ids.Contains(temp_id))
            {
                temp_id = GetRandomFactId();

            }
            _used_fact_ids.Add(temp_id);

            return _Nodes.Find(x => x.internal_id == temp_id);
        }

        private int GetNumberOfPopulatedRuleOfFacts(Node rule)
        {
            int result = 0;
            foreach (var item in rule.Neighbours)
            {
                switch (item.Weight)
                {
                    case 1: result++; break;
                    case 0: break;
                    case -1: return -1;

                }
            }
            return result;
        }



        private Node GetTheMostPopulatedFact()
        {
            int last_node_populated = -1;
            Node result = null;
            List<Node> ListToRemove = new List<Node>(); ;

            var allFactNodes = _Nodes.Where(x => x.GetType() == typeof(FactNode));
            if (allFactNodes.Count() ==0)
            {
                throw new KeyNotFoundException();
            }

            foreach (var fact in allFactNodes)
            {

                int numberofpopulatedfacts = GetNumberOfPopulatedRuleOfFacts(fact);
                if (numberofpopulatedfacts == -1)
                {
                    ListToRemove.Add(fact);
                }
                else
                {
                    if (numberofpopulatedfacts > last_node_populated)
                        result = fact;
                    last_node_populated = -1;
                }
            }

            foreach (var item in ListToRemove)
            {
                _Nodes.Remove(item);
            }

            return result;
        }
        private Node GetAnotherQuestionsOfFact(Node Fact)
        {
            var list = Fact.Neighbours.Where(x => x.Weight == 0);
            var first = list.FirstOrDefault();
            return first;
        }

        public Node GetQuestion()
        {
            if (Attempts_Tracker == 0)
            {
                Attempts_Tracker++;
                return GetRandomRule();
            }

            else

                return GetAnotherQuestionsOfFact(GetTheMostPopulatedFact());



        }

      

        public Node CheckIfThereIsResult()
        {
            var most_populated = GetTheMostPopulatedFact();
            int neighbours = most_populated.Neighbours.Count;
            int populated = GetNumberOfPopulatedRuleOfFacts(most_populated);
            if (neighbours == populated)
                return most_populated;
            else
                return null;


        }
        public void UpdateWeight(int internal_id, int value)
        {
            var n = _Nodes.Single(x => x.internal_id == internal_id);
            n.Weight = value;
        }



    }
}