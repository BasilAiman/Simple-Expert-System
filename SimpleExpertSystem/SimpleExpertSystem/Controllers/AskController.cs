﻿using SimpleExpertSystem.DataStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleExpertSystem.Controllers
{
    public class AskController : Controller
    {
        
        // GET: Ask
        public ActionResult Index()
        {
            try
            {
                var Node = MvcApplication.Map.GetQuestion();
                return View(Node);
            }
            catch (Exception)
            {
                return RedirectToAction("NotFound");
                
            }
        }
        [HttpPost]
        public ActionResult Index(int internal_id , int value)
        {
            try
            {

                MvcApplication.Map.UpdateWeight(internal_id, value);

                var result = MvcApplication.Map.CheckIfThereIsResult();
                if (result != null)
                    return RedirectToAction("Result");
                else
                    return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return RedirectToAction("NotFound");
            }

        }

        public ActionResult Result()
        {
            var result = MvcApplication.Map.CheckIfThereIsResult();
            MvcApplication.Map.Reset();
            return View(result);

        }
        public ActionResult NotFound()
        {
            return View();
        }
    }
}