﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SimpleExpertSystem.Models;

namespace SimpleExpertSystem.Controllers
{
    public class FactsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Facts
        public ActionResult Index()
        {
            return View(db.Facts.ToList());
        }

        // GET: Facts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facts facts = db.Facts.Find(id);
            if (facts == null)
            {
                return HttpNotFound();
            }
            return View(facts);
        }

        // GET: Facts/Create
        public ActionResult Create()
        {
            ViewBag.Rules = db.Rules;
            return View();
        }

        // POST: Facts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Text")] Facts facts, List<int> rules)
        {
            if (ModelState.IsValid)
            {
                facts.UsedRules = db.Rules.Where(x => rules.Contains(x.id)).ToList();
                db.Facts.Add(facts);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(facts);
        }

        // GET: Facts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facts facts = db.Facts.Find(id);
            if (facts == null)
            {
                return HttpNotFound();
            }
            ViewBag.Rules = db.Rules;

            return View(facts);
        }

        // POST: Facts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Text")] Facts facts, List<int> rules)
        {
            if (ModelState.IsValid)
            {
                facts.UsedRules = db.Rules.Where(x => rules.Contains(x.id)).ToList();
                db.Entry(facts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(facts);
        }

        // GET: Facts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facts facts = db.Facts.Find(id);
            if (facts == null)
            {
                return HttpNotFound();
            }
            return View(facts);
        }

        // POST: Facts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Facts facts = db.Facts.Find(id);
            db.Facts.Remove(facts);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
